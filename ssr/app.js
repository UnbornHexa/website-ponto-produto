/* Requires */

const express = require('express')
const path = require('path')
const morgan = require('morgan')
const serveStatic = require('serve-static')
const compression = require('compression')
const helmet = require('helmet')

const app = express()

/* Middlewares */

app.use('/static', serveStatic(path.join(__dirname, '../build')))
app.use('/', serveStatic(path.join(__dirname, '../build/public')))

app.use(compression())
app.use(morgan('common'))

app.use(helmet.hidePoweredBy())

/* Routes */

const INDEX_ROUTE = require('./routes/index')
const ERRO_ROUTE = require('./routes/erro')

app.use('/', INDEX_ROUTE)
app.use('/', ERRO_ROUTE)

module.exports = app
