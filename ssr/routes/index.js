/* Requires */

const express = require('express')
const router = express.Router()

/* Controllers */

const INDEX_CONTROLLER = require('../controllers/index')

/* Methods */

router.get('/', INDEX_CONTROLLER.renderizar)

module.exports = router
