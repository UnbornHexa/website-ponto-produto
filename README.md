1. Baixando o Projeto
```
git clone https://gitlab.com/ponto-produto/frontend/website.git
```

2. Instalando as Dependências
```
yarn install
```

3. Executando a Aplicação
```
yarn run dev
```
