const Module = () => {
  const methods = {}

  // Internal Variables

  const textoAprender = document.querySelector('.texto_aprender')
  const divAviso = document.querySelector('.aviso')

  // Methods

  methods.habilitarAviso = () => {
    textoAprender.addEventListener('click', () => {
      divAviso.classList.add('mostrar');
    })
  }

  return methods
}

module.exports = Module
