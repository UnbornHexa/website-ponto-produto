const Module = () => {
  const methods = {}

  // Internal Variables

  const sectionLoading = document.querySelector('section.loading')

  // Methods

  methods.habilitarOcultacaoLoading = () => {
    window.addEventListener('load', () => {
      const tempoEspera = 1 * 3000
      const ocultarLoading = () => sectionLoading.classList.add('ocultar')
      setTimeout(() => ocultarLoading(), tempoEspera)
    })
  }

  return methods
}

module.exports = Module
