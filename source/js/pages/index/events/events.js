/* Requires */

const EVENTS = require('../../../global/events/events')
const SECTION_LOADING = require('./section-loading')
const AVISO_APRENDER = require('./aviso')
const HEADER = require('./header')

/* Module */

const Module = () => {
  const methods = {}

  methods.habilitarTodosOsEventos = () => {
    SECTION_LOADING().habilitarOcultacaoLoading()
    AVISO_APRENDER().habilitarAviso()
    HEADER().habilitarCursor()

    EVENTS().habilitarTodosOsEventosGlobais()
  }

  return methods
}

module.exports = Module
