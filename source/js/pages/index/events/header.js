const Module = () => {
  const methods = {}

  // Internal Variables

  const linkAprender = document.querySelector('.aprender a.link')
  const linkFazer = document.querySelector('.fazer a.link')
  const cursor = document.querySelector('.cursor')

  // Methods

  methods.habilitarCursor = () => {
    const ativarEstilo = () => cursor.classList.add('cursor_link')
    const removerEstilo = () => cursor.classList.remove('cursor_link')

    linkAprender.addEventListener('mouseover', () => ativarEstilo())
    linkAprender.addEventListener('mouseleave', () => removerEstilo())
    linkFazer.addEventListener('mouseover', () => ativarEstilo())
    linkFazer.addEventListener('mouseleave', () => removerEstilo())
  }

  return methods
}

module.exports = Module
