/* Requires */

const EVENTS = require('../../../global/events/events')

/* Module */

const Module = () => {
  const methods = {}

  methods.habilitarTodosOsEventos = () => {
    EVENTS().habilitarTodosOsEventosGlobais()
  }

  return methods
}

module.exports = Module
