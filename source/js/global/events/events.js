/* Requires */

const WINDOW = require('./window')
const CURSOR = require('./cursor')

/* Module */

const Module = () => {
  const methods = {}

  methods.habilitarTodosOsEventosGlobais = () => {
    WINDOW().habilitarBloqueioDragAndDrop()
    CURSOR().habilitarCursor()
  }

  return methods
}

module.exports = Module
