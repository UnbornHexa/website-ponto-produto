const Module = () => {
  const methods = {}

  // Methods

  methods.habilitarCursor = () => {
    const mouseCursor = document.querySelector('.cursor')

    window.addEventListener('mousemove', cursor);

    function cursor(e) {
      mouseCursor.style.top = e.pageY + 'px';
      mouseCursor.style.left = e.pageX + 'px';
    }
  }

  return methods
}

module.exports = Module
