const Module = () => {
  const methods = {}

  // Methods

  methods.habilitarBloqueioDragAndDrop = () => {
    document.addEventListener('dragstart', (evento) => evento.preventDefault(), false)
    document.addEventListener('drop', (evento) => evento.preventDefault(), false)
  }

  return methods
}

module.exports = Module
