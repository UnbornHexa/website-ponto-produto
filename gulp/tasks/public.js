/* Requires */

const gulp = require('gulp')

const notify = require('gulp-notify')
const plumber = require('gulp-plumber')
const path = require('path')

/* Settings */

const errorMessage = 'Error: <%= error.message %>'
const route = {
  source: path.join(__dirname, '../../source/public'),
  build: path.join(__dirname, '../../build/public')
}

/* Tasks */

gulp.task('bundle-public', function () {
  return gulp.src(route.source + '/**')
    .pipe(plumber({ errorHandler: error => { notify.onError(errorMessage)(error) } }))
    .pipe(gulp.dest(route.build))
})

gulp.task('app.public', gulp.parallel([
  'bundle-public'
]))
